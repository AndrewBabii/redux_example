
import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux_example/drawer/main_drawer_actions.dart';
import 'package:redux_example/routes.dart';
import 'package:redux_example/store/app/reducer.dart';

class MainDrawerState {
  final double counter;

  final bool isAppBarColorActive;
  final Color backgroundColor;

  MainDrawerState({
    this.counter,
    this.isAppBarColorActive,
    this.backgroundColor,
  });

  factory MainDrawerState.initial() {
    return MainDrawerState(
      counter: 0,
      isAppBarColorActive: true,
      backgroundColor: Colors.green,
    );
  }

  MainDrawerState copyWith({
    double counter,
    bool isAppBarColorActive,
    Color backgroundColor,
  }) {
    return MainDrawerState(
      counter: counter ?? this.counter,
      isAppBarColorActive: isAppBarColorActive ?? this.isAppBarColorActive,
      backgroundColor: backgroundColor ?? this.backgroundColor,
    );
  }

  MainDrawerState reducer(dynamic action) {
    return Reducer<MainDrawerState>(
      actions: HashMap.from({
        ClearCounterAction: (dynamic action) => _clearCounter(),
        ChangeAppBarColorActive: (dynamic action) => _changeAppBarColorActive(action as ChangeAppBarColorActive),
        ChangeBackgroundColor: (dynamic action) => _changeBackgroundColor(action as ChangeBackgroundColor),
        GoToMainScreen: (dynamic action) => _goToMainScreen(action as GoToMainScreen),
        GoToSecondScreen: (dynamic action) => _goToSecondScreen(action as GoToSecondScreen),
      }),
    ).updateState(action, this);
  }

  MainDrawerState _clearCounter() {
    return copyWith(counter: 0);
  }

  MainDrawerState _changeAppBarColorActive(ChangeAppBarColorActive action) {
    return copyWith(isAppBarColorActive: action.isAppBarColorActive);
  }

  MainDrawerState _changeBackgroundColor(ChangeBackgroundColor action) {
    return copyWith(backgroundColor: action.backgroundColor);
  }

  void _goToMainScreen(GoToMainScreen action){
    NavigateToAction(Routes.kSecondPage);
  }

  void _goToSecondScreen(GoToSecondScreen action){
    NavigateToAction(Routes.kMainPage);
  }
}
