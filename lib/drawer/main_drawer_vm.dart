import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:redux_example/drawer/main_drawer_selectors.dart';
import 'package:redux_example/store/app/app_state.dart';

class MainDrawerViewModel {
  final void Function() goToMainScreen;
  final void Function() goToSecondScreen;
  final void Function() goToThirdScreen;
  final void Function() goToForthScreen;

  final bool isAppBarColorActive;
  final Color backgroundColor;

  final void Function() clearCounter;

  final void Function(bool) changeAppBarColorActive;
  final void Function(Color) changeBackgroundColor;

  MainDrawerViewModel({
    @required this.goToMainScreen,
    @required this.goToSecondScreen,
    @required this.goToThirdScreen,
  @required this.goToForthScreen,
    @required this.isAppBarColorActive,
    @required this.backgroundColor,
    @required this.clearCounter,
    @required this.changeAppBarColorActive,
    @required this.changeBackgroundColor,
  });

  static MainDrawerViewModel fromStore(Store<AppState> store) {
    return MainDrawerViewModel(
      goToMainScreen: MainDrawerSelectors.goToMainScreen(store),
      goToSecondScreen: MainDrawerSelectors.goToSecondScreen(store),
      goToForthScreen: MainDrawerSelectors.goToSecondScreen(store),
      goToThirdScreen: MainDrawerSelectors.goToSecondScreen(store),
      isAppBarColorActive: MainDrawerSelectors.getAppBarColorValue(store),
      backgroundColor: MainDrawerSelectors.getBackgroundColorValue(store),
      clearCounter: MainDrawerSelectors.getClearCounter(store),
      changeAppBarColorActive: MainDrawerSelectors.changeAppBarColorActive(store),
      changeBackgroundColor: MainDrawerSelectors.changeBackgroundColor(store),
    );
  }
}
