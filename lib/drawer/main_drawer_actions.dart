import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class GoToMainScreen{

}

class GoToSecondScreen{

}

class ClearCounterAction{

}

class ChangeAppBarColorActive{
  final bool isAppBarColorActive;

  ChangeAppBarColorActive({@required this.isAppBarColorActive});
}

class ChangeBackgroundColor{
  final Color backgroundColor;

  ChangeBackgroundColor({@required this.backgroundColor});
}