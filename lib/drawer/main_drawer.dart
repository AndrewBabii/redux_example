import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux_example/counter_page_viewmodel.dart';
import 'package:redux_example/drawer/main_drawer_vm.dart';
import 'package:redux_example/routes.dart';
import 'package:redux_example/store/app/app_state.dart';

class MainDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, MainDrawerViewModel>(
        converter: MainDrawerViewModel.fromStore,
        builder: (BuildContext context, MainDrawerViewModel viewModel) {
          bool switchValue = viewModel.isAppBarColorActive;
          return Drawer(
            child: Column(
              children: [
                Container(
                  height: 100,
                ),
                Switch(
                  value: switchValue,
                  onChanged: (val) => viewModel.changeAppBarColorActive(val),
                ),
                FlatButton(
                  onPressed: viewModel.clearCounter,
                  child: Text('Clear Counter'),
                ),
                FlatButton(
                  onPressed: () => viewModel.changeBackgroundColor(Colors.green),
                  child: Text('Green'),
                  color: Colors.green,
                ),
                FlatButton(
                  onPressed: () => viewModel.changeBackgroundColor(Colors.red),
                  child: Text('Red'),
                  color: Colors.red,
                ),
                FlatButton(
                  onPressed: () => viewModel.changeBackgroundColor(Colors.blue),
                  child: Text('Blue'),
                  color: Colors.blue,
                ),
                FlatButton(
                  onPressed: () =>  viewModel.goToSecondScreen(),
                  child: Text('go to second'),
                  color: Colors.blue,
                ),
                FlatButton(
                  onPressed: () => viewModel.goToMainScreen(),
                  child: Text('go to main'),
                  color: Colors.blue,
                ),
                FlatButton(
                  onPressed: () => viewModel.goToMainScreen(),
                  child: Text('go to Third'),
                  color: Colors.blue,
                ),
                FlatButton(
                  onPressed: () => viewModel.goToMainScreen(),
                  child: Text('go to forth'),
                  color: Colors.blue,
                ),
              ],
            ),
          );
        });
  }
}
