import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:redux_example/drawer/main_drawer_actions.dart';
import 'package:redux_example/store/app/app_state.dart';

class MainDrawerSelectors {


  static void Function() goToMainScreen(Store<AppState> store) {
    return () => store.dispatch(GoToMainScreen());
  }

  static void Function() goToSecondScreen(Store<AppState> store) {
    return () => store.dispatch(GoToSecondScreen());
  }

  static bool getAppBarColorValue(Store<AppState> store) {
    return store.state.counterPageState.isAppBarColorActive;
  }

  static Color getBackgroundColorValue(Store<AppState> store) {
    return store.state.counterPageState.backgroundColor;
  }

  static void Function() getClearCounter(Store<AppState> store) {
    return () => store.dispatch(ClearCounterAction());
  }

  static void Function(bool isAppBarCounterActive) changeAppBarColorActive(Store<AppState> store) {
    return (bool isAppBarCounterActive) => store.dispatch(ChangeAppBarColorActive(isAppBarColorActive: isAppBarCounterActive));
  }

  static void Function(Color backgroundColor) changeBackgroundColor(Store<AppState> store) {
    return (Color color) => store.dispatch(ChangeBackgroundColor(backgroundColor: color));
  }

}