import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'package:redux_example/counter_page_viewmodel.dart';
import 'package:redux_example/store/app/app_state.dart';
import 'package:redux_epics/redux_epics.dart';
import 'file:///C:/Users/AppVesto/AndroidStudioProjects/redux_example/lib/drawer/main_drawer.dart';

void main() {
  Store store = Store<AppState>(
    AppState.getAppReducer,
    initialState: AppState.initial(),
    middleware: [
      // EpicMiddleware(AppState.getAppEpic),
      NavigationMiddleware<AppState>(),
    ],
  );
  runApp(MyApp(
    store: store,
  ));
}

class MyApp extends StatelessWidget {
  final Store<AppState> store;

  MyApp({@required this.store});

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: MyHomePage(title: 'Flutter Demo Home Page'),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  Widget build(BuildContext context) {

    return StoreConnector<AppState, CounterPageViewModel>(
      converter: CounterPageViewModel.fromStore,
      builder: (BuildContext context, CounterPageViewModel viewModel) {
        bool switchValue = viewModel.isAppBarColorActive;
        return Scaffold(
          appBar: AppBar(
            backgroundColor: viewModel.isAppBarColorActive ? Colors.orange : Colors.grey,
            title: Text(title),
          ),
          body: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                IconButton(
                  icon: Icon(
                    Icons.remove,
                    color: Colors.red,
                  ),
                  onPressed: viewModel.decrementCounter,
                ),
                Text(
                  viewModel.counter.toString(),
                  style: Theme.of(context).textTheme.headline4,
                ),
                IconButton(
                  icon: Icon(
                    Icons.add,
                    color: Colors.green,
                  ),
                  onPressed: viewModel.incrementCounter,
                ),
                IconButton(
                  icon: Icon(
                    Icons.star,
                    color: Colors.green,
                  ),
                  onPressed: () => viewModel.multiplyCounter(viewModel.counter),
                ),
              ],
            ),
          ),
          drawer: MainDrawer(),
          backgroundColor: viewModel.backgroundColor,
        );
      },
    );
  }
}
