import 'package:flutter/material.dart';
import 'package:redux_example/forht_page.dart';
import 'package:redux_example/main.dart';
import 'package:redux_example/routes.dart';
import 'package:page_transition/page_transition.dart';
import 'package:redux_example/store/app/second_page.dart';
import 'package:redux_example/third_page.dart';

class RouteService {
  RouteService._privateConstructor();

  static final RouteService _instance = RouteService._privateConstructor();

  static RouteService get instance => _instance;

  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    print(settings.name);
    switch (settings.name) {
      case Routes.kMainPage:
        {
          return PageTransition(
            child: MyHomePage(),
            type: PageTransitionType.fade,
            settings: settings,
          );
        }
      case Routes.kSecondPage:
        {
          return PageTransition(
            child: SecondPage(),
            type: PageTransitionType.fade,
            settings: settings,
          );
        }
      case Routes.kForthPage:
        {
          return PageTransition(
            child: ForthPage(),
            type: PageTransitionType.fade,
            settings: settings,
          );
        }
      case Routes.kThirdPage:
        {
          return PageTransition(
            child: ThirdPage(),
            type: PageTransitionType.fade,
            settings: settings,
          );
        }
      default:
        return _defaultRoute(
          settings: settings,
          page: MyHomePage(),
        );
    }
  }

  static MaterialPageRoute _defaultRoute({RouteSettings settings, Widget page}) {
    return MaterialPageRoute(
      settings: settings,
      builder: (BuildContext context) => page,
    );
  }
}
