import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:redux/redux.dart';
import 'package:redux_example/store/app/app_state.dart';
import 'package:redux_example/store/app/counter_page_state/counter_page_selectors.dart';


class CounterPageViewModel {
  final double counter;
  final bool isAppBarColorActive;
  final Color backgroundColor;

  final void Function() incrementCounter;
  final void Function() decrementCounter;
  final void Function() clearCounter;
  final void Function(double) multiplyCounter;
  final void Function(bool) changeAppBarColorActive;
  final void Function(Color) changeBackgroundColor;

  CounterPageViewModel({
    @required this.counter,
    @required this.isAppBarColorActive,
    @required this.backgroundColor,
    @required this.incrementCounter,
    @required this.decrementCounter,
    @required this.multiplyCounter,
    @required this.clearCounter,
    @required this.changeAppBarColorActive,
    @required this.changeBackgroundColor,
  });

  static CounterPageViewModel fromStore(Store<AppState> store) {
    return CounterPageViewModel(
      counter: CounterPageSelectors.getCounterValue(store),
      isAppBarColorActive: CounterPageSelectors.getAppBarColorValue(store),
      backgroundColor:  CounterPageSelectors.getBackgroundColorValue(store),
      incrementCounter: CounterPageSelectors.getIncrementCounterFunction(store),
      decrementCounter: CounterPageSelectors.getDecrementCounterFunction(store),
      multiplyCounter: CounterPageSelectors.getMultiplyCounterFunction(store),
      clearCounter: CounterPageSelectors.getClearCounter(store),
      changeAppBarColorActive: CounterPageSelectors.changeAppBarColorActive(store),
      changeBackgroundColor: CounterPageSelectors.changeBackgroundColor(store),
    );
  }
}
