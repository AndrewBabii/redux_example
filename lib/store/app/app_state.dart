import 'package:flutter/foundation.dart';
import 'package:redux_example/store/app/counter_page_state/counter_page_state.dart';
import 'package:redux_epics/redux_epics.dart';
import 'file:///C:/Users/AppVesto/AndroidStudioProjects/redux_example/lib/drawer/main_drawer_state.dart';

class AppState {
  final CounterPageState counterPageState;
  final MainDrawerState mainDrawerState;

  AppState({
    @required this.counterPageState,
    @required this.mainDrawerState,
  });

  factory AppState.initial() {
    return AppState(
      counterPageState: CounterPageState.initial(),
      mainDrawerState: MainDrawerState.initial(),
    );
  }

  static AppState getAppReducer(AppState state, dynamic action) {
    const String TAG = '[appReducer]';
    print('$TAG => <appReducer> => action: ${action.runtimeType}');
    return AppState(
      counterPageState: state.counterPageState.reducer(action),
      mainDrawerState: state.mainDrawerState.reducer(action),
    );
  }

// static final getAppEpic = combineEpics<AppState>([]);
}
