
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:redux_example/store/app/app_state.dart';
import 'package:redux_example/store/app/counter_page_state/counter_page_actions.dart';

class CounterPageSelectors {

  static double getCounterValue(Store<AppState> store) {
    return store.state.counterPageState.counter;
  }

  static bool getAppBarColorValue(Store<AppState> store){
    return store.state.counterPageState.isAppBarColorActive;
  }

  static Color getBackgroundColorValue(Store<AppState> store){
    return store.state.counterPageState.backgroundColor;
  }

  static void Function() getIncrementCounterFunction(Store<AppState> store) {
    return () => store.dispatch(IncrementAction());
  }

  static void Function() getDecrementCounterFunction(Store<AppState> store) {
    return () => store.dispatch(DecrementAction());
  }

  static void Function(double counter) getMultiplyCounterFunction(Store<AppState> store) {
    return (double counter) => store.dispatch(MultiplyCounterAction(counter2: counter));
  }

  static void Function() getClearCounter(Store<AppState> store){
    return () =>store.dispatch(ClearCounterAction());
  }

  static void Function(bool isAppBarCounterActive) changeAppBarColorActive(Store<AppState> store){
    return (bool isAppBarCounterActive) => store.dispatch(ChangeAppBarColorActive(isAppBarColorActive: isAppBarCounterActive));
  }

  static void Function(Color backgroundColor) changeBackgroundColor(Store<AppState> store){
    return(Color color) => store.dispatch(ChangeBackgroundColor(backgroundColor: color));
  }
}