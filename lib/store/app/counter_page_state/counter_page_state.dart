import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:redux_example/store/app/counter_page_state/counter_page_actions.dart';
import 'package:redux_example/store/app/reducer.dart';


class CounterPageState {
  final double counter;
  final double counter2;
  final bool isAppBarColorActive;
  final Color backgroundColor;


  CounterPageState({
    this.counter,
    this.counter2,
    this.isAppBarColorActive,
    this.backgroundColor,
  });

  factory CounterPageState.initial() {
    return CounterPageState(
      counter: 0,
      counter2: 10,
      isAppBarColorActive: true,
      backgroundColor: Colors.green,
    );
  }

  CounterPageState copyWith({
    double counter,
    double counter2,
    bool isAppBarColorActive,
    Color backgroundColor,
  }) {
    return CounterPageState(
      counter: counter ?? this.counter,
      counter2: counter2 ?? this.counter2,
      isAppBarColorActive:isAppBarColorActive ?? this.isAppBarColorActive,
      backgroundColor: backgroundColor ?? this.backgroundColor,
    );
  }

  CounterPageState reducer(dynamic action) {
    return Reducer<CounterPageState>(
      actions: HashMap.from({
        IncrementAction: (dynamic action) => _incrementCounter(),
        DecrementAction: (dynamic action) => _decrementCounter(),
        MultiplyCounterAction: (dynamic action) => _multiplyCounter(action as MultiplyCounterAction),
        ClearCounterAction: (dynamic action) => _clearCounter(),
        ChangeAppBarColorActive: (dynamic action) => _changeAppBarColorActive(action as ChangeAppBarColorActive),
        ChangeBackgroundColor: (dynamic action) => _changeBackgroundColor(action as ChangeBackgroundColor),
      }),
    ).updateState(action, this);
  }

  CounterPageState _incrementCounter() {
    return CounterPageState(counter: counter+1);
  }

  CounterPageState _decrementCounter() {
    return copyWith(counter: counter-1);

  }

  CounterPageState _multiplyCounter(MultiplyCounterAction action){
    return copyWith(counter: action.counter2 * 2);
  }

  CounterPageState _clearCounter(){
    return copyWith(counter: 0);
  }

  CounterPageState _changeAppBarColorActive(ChangeAppBarColorActive action){

    return copyWith(isAppBarColorActive: action.isAppBarColorActive);
  }

  CounterPageState _changeBackgroundColor(ChangeBackgroundColor action){
    return copyWith(backgroundColor: action.backgroundColor);
  }

}