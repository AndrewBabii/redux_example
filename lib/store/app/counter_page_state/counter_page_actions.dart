

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class IncrementAction {
  IncrementAction();
}

class DecrementAction {

}

class ClearCounterAction{

}

class ChangeAppBarColorActive{
final bool isAppBarColorActive;

ChangeAppBarColorActive({@required this.isAppBarColorActive});
}

class MultiplyCounterAction {
  final double counter2;

  MultiplyCounterAction({@required this.counter2});
}

class ChangeBackgroundColor{
  final Color backgroundColor;

  ChangeBackgroundColor({@required this.backgroundColor});
}